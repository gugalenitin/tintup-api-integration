angular
    .module('tintupIntegrationApp', [
        'ui.router',
        'infinite-scroll'
    ]);

angular
    .module('tintupIntegrationApp')
    .config(function($locationProvider, $stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('feed', {
                url: '/feed',
                controller: 'FeedCtrl',
                templateUrl: 'src/components/feed/feed.tpl.html'
            })

        $urlRouterProvider.otherwise('/feed');
    });

angular
    .module('tintupIntegrationApp')
    .value('THROTTLE_MILLISECONDS', 2000);