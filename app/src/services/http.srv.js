angular
    .module('tintupIntegrationApp')
    .constant('httpServiceConstant', {
        apiToken: 'ada602da1d7689df23bf3666dd575322711497cc',
        apiBase: 'https://api.tintup.com/v1/',
        httpSuccessCode: 200
    })
    .factory('httpService', function($http, $q, $log, httpServiceConstant) {
        return {
            get: function(path, isRelative) {
                var deferred = $q.defer(),
                    url = isRelative
                    ? httpServiceConstant.apiBase + path + '?api_token=' + httpServiceConstant.apiToken
                    : path;
                $log.debug('API request started - ' + url);
                $http
                    .get(url)
                    .then(function(response) {
                        $log.debug('API request completed - ' + url + ' with status code - ' + response.status);
                        if (response.status === httpServiceConstant.httpSuccessCode) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject('Invalid HTTP response status - ' + response.status);
                        }
                    }, function(error) {
                        $log.error('API request completed - ' + url + ' with error - ' + error);
                        deferred.reject(error);
                    })

                return deferred.promise;
            }
        };
    });