angular
    .module('tintupIntegrationApp')
    .constant('feedServiceConstant', {
        feedEndpoint: 'feed/tint/'
    })
    .factory('feedService', function(httpService, feedServiceConstant) {
        return {
            getFeeds: function(path) {
                var url = feedServiceConstant.feedEndpoint,
                    isRelative = true;

                if (path) {
                    url = path;
                    isRelative = false;
                }
                return httpService.get(url, isRelative);
            }
        };
    });