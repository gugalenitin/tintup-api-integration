angular
    .module('tintupIntegrationApp')
    .constant('feedCtrlConstant', {
        tilesPerRow: [3, 5, 4]
    })
    .controller('FeedCtrl', function($scope, feedService, feedCtrlConstant) {
        
        var init = function() {
            $scope.feeds = [];
            $scope.nextPage = '';
            $scope.loadingFeeds = false;
            $scope.fetchNextFeeds = fetchNextFeeds;
        }

        var fetchFeeds = function(url) {
            if ($scope.loadingFeeds) {
                return;
            }
            $scope.loadingFeeds = true;
            feedService
                .getFeeds(url)
                .then(function(response) {
                    $scope.feeds = $scope.feeds.concat(formatFeeds(response.data));
                    $scope.nextPage = response.next_page;
                    $scope.loadingFeeds = false;
                }, function(error) {
                    $scope.loadingFeeds = false;
                });
        }

        var formatFeeds = function(feeds) {
            var formattedFeeds = [],
                currentTile = 0,
                tilesPerRowIndex = 0,
                innerFeeds = [];
            feeds.forEach(function(feed) {
                innerFeeds.push(feed);
                currentTile++;
                if (currentTile % feedCtrlConstant.tilesPerRow[tilesPerRowIndex] === 0) {
                    formattedFeeds.push(innerFeeds);
                    innerFeeds = [];
                    currentTile = 0;
                    tilesPerRowIndex++;
                    if (tilesPerRowIndex === feedCtrlConstant.tilesPerRow.length) {
                        tilesPerRowIndex = 0;
                    }
                }
            });
            if (innerFeeds.length > 0) {
                formattedFeeds.push(innerFeeds);
            }
            return formattedFeeds;
        }

        var fetchNextFeeds = function() {
            if ($scope.nextPage) {
                fetchFeeds($scope.nextPage);
            }
        }

        init();
        fetchFeeds();
    });