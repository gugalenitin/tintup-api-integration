# TintUp API Integration #
## 1.0.0 ##

This is a smaple Angular application to integrate TintUp API

## Requirements ##

* Node.js - https://nodejs.org/en/download/

## How to run? ##

  * Install node dependencies - ```npm install```
  * Install bower dependencies - ```bower install```
  	* If you get bower not found error then run following command - ```npm install -g bower```
  * Run App - ```gulp connect```
    * If you get gulp not found error then run following command - ```npm install -g gulp```
	
Once the server is up, you may connect to http://localhost:8080 
